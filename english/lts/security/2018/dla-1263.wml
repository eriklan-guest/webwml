<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update marks several packages as no longer supported by wheezy-lts:</p>

<p>teamspeak-server, teamspeak-client, libstruts1.2-java, nvidia-graphics-drivers,
glassfish, jbossas4, libnet-ping-external-perl, mp3gain, tor,
jasperreports.</p>

<p>For the reasoning please see the links provided in</p>

    <p>/usr/share/debian-security-support/security-support-ended.deb8</p>

<p>Furthermore it marks swftools as only safe to use for trusted input.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2018.01.29~deb7u1.</p>

<p>We recommend that you upgrade your debian-security-support packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1263.data"
# $Id: $
