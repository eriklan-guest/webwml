<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17100">CVE-2018-17100</a>
      <p>An int32 overflow can cause a denial of service (application
      crash) or possibly have unspecified other impact via a crafted
      image file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17101">CVE-2018-17101</a>

      <p>Out-of-bounds writes can cause a denial of service (application
      crash) or possibly have unspecified other impact via a crafted
      image file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18557">CVE-2018-18557</a>

      <p>Out-of-bounds write due to ignoring buffer size can cause a denial
      of service (application crash) or possibly have unspecified other
      impact via a crafted image file</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.0.3-12.3+deb8u7.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1557.data"
# $Id: $
