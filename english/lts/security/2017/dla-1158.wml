<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in bchunk, a tool to convert a
CD image in .bin/.cue format into a set of .iso and .cdr/.wav tracks.
It was possible to trigger a heap-based buffer overflow with an
resultant invalid free when processing a malformed CUE (.cue) file
that may lead to the execution of arbitrary code or a application crash.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.2.0-12+deb7u1.</p>

<p>We recommend that you upgrade your bchunk packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1158.data"
# $Id: $
