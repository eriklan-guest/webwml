<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the GD Graphics Library.
They may lead to the execution of arbitrary code or causing
application crash.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9317">CVE-2016-9317</a>

    <p>Signed integer overflow in gd_io.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10167">CVE-2016-10167</a>

    <p>Improper handling of issing image data can cause crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10168">CVE-2016-10168</a>

    <p>GD2 stores the number of horizontal and vertical chunks as words
    (i.e. 2 byte unsigned). These values are multiplied and assigned to
    an int when reading the image, what can cause integer overflows.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.0.36~rc1~dfsg-6.1+deb7u8.</p>

<p>We recommend that you upgrade your libgd2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-804.data"
# $Id: $
